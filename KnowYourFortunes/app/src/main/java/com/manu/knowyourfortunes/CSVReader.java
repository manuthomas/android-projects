//Handles read and checking of credentials

package com.manu.knowyourfortunes;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CSVReader {
    Context context;
    String fileName;
    List<String[]> rows = new ArrayList<>();
    Boolean matched = false;

    public CSVReader(Context context, String fileName) {
        this.context = context;
        this.fileName = fileName;
    }

    public boolean readCSV(String name, String regid) throws IOException {
        InputStream is = context.getAssets().open(fileName);
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String line;
        String csvSplitBy = ",";

        while ((line = br.readLine()) != null) {
            String[] row = line.split(csvSplitBy);
            if(row[1].equals(regid)){
                if(row[0].equals(name)){
                    return true;
                }else{
                    return false;
                }
            }

        }
        return matched;
    }
}
