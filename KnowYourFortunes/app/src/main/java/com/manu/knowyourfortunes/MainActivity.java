//Checks credentials and takes to fortune page

package com.manu.knowyourfortunes;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    Intent intent;
    public void authenticate(View view){
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        //Class to read from csv file
        CSVReader csvReader = new CSVReader(MainActivity.this, "InputCredentials.csv");

        EditText name = (EditText) findViewById(R.id.name);
        EditText regid = (EditText) findViewById(R.id.regid);
        //Checking Login
        try {
            boolean authenticated = csvReader.readCSV(name.getText().toString(),regid.getText().toString());
            if(authenticated){      //On success takes you to Fortune page
                intent.putExtra("Name",name.getText().toString());
                startActivity(intent);
            }else{
                Toast.makeText(this, "Authentication Failed", Toast.LENGTH_LONG).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        intent = new Intent(getApplicationContext(),FortuneActivity.class);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
    }
}
