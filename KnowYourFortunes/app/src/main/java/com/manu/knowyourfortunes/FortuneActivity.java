package com.manu.knowyourfortunes;


import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class FortuneActivity extends AppCompatActivity {
    ArrayList<String> fortunes;
    AlertDialog.Builder prompt;

    public void findFortune(){                          //Prints out the fortune


        Random ran = new Random();
        int x = ran.nextInt(fortunes.size()) + 0;
        new AlertDialog.Builder(this)
                .setTitle(fortunes.get(x))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        prompt.show();
                    }
                })
                .show();
    }

    //On creating, shows initial fortune

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fortune);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        //List of fortunes

        List<String> fortuneStrings = Arrays.asList("You will get 9.0 CGPA this semester.",
                "Happiness is programming",
                "Satisfaction follows hard work",
                "Patience is virtue",
                "Work hard, party harder",
                "Where you go success follows",
                "Stay away from flying saucers today",
                "Beware of Bigfoot!",
                "You will be misunderstood by everyone.",
                "What happened last night can happen again.",
                "You will win success in whatever calling you adopt.",
                "Good day for overcoming obstacles.  Try a steeplechase.",
                "Do nothing unless you must, and when you must act -- hesitate.");

        fortunes = new ArrayList<String>();
        fortunes.addAll(fortuneStrings);


        prompt = new AlertDialog.Builder(this)
                .setTitle("Your fortune?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                findFortune();
                            }
                        }, 500);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                });
        findFortune();


    }
}
